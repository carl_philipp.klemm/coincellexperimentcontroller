#include <stdexcept>

class startup_failure: public std::runtime_error
{
public:
	startup_failure(const std::string& what_arg): std::runtime_error(what_arg)
	{
	}
};
