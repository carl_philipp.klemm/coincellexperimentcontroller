#pragma once
#include <libserialport.h>
#include <string>
#include <mutex>

class Vcpps
{
public:

	struct Status
	{
		float voltage;
		float current;
		bool curent_limited;
	};

private:
	struct sp_port* port = nullptr;
	std::mutex portMutex;

	inline static const char* COMMAND_OUTPUT_DISABLE = "SOUT";
	inline static const char* COMMAND_VOLTAGE = "VOLT";
	inline static const char* COMMAND_CURRENT = "CURR";
	inline static const char* COMMAND_STATUS = "GETD";

	bool waitForOk();

public:
	Vcpps(const std::string& portName);
	~Vcpps();
	Vcpps(const Vcpps& in);
	Vcpps& operator=(const Vcpps& in);

	bool setEnabled(bool enabled);
	bool setVoltage(float voltage);
	bool setCurrent(float current);
	bool getStatus(Status& status);
};
