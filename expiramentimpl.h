#include "expirament.h"
#include <memory>
#include <algorithm>

#include "randomgen.h"

bool Expirament::step(size_t startsubstep)
{
	static constexpr int CYCLES_PER_STEP = 4;

	if(startsubstep >= CYCLES_PER_STEP)
		Log(Log::WARN)<<"Substep "<<startsubstep<<" given but there are only "<<CYCLES_PER_STEP<<" steps";

	saveStep(startsubstep);

	std::vector<double> steps = createStepSequence(3, 2, 0, 1);
	std::vector<double> chargeThermalSteps(6);
	std::for_each(chargeThermalSteps.begin(), chargeThermalSteps.end(), [](double& in){in = rd::rand(1, 0);});

	bool ret = true;
	switch(globalstep % STEP_COUNT)
	{
		case 0:
			if((globalstep/STEP_COUNT) % 10 == 0)
			{
				ret = capacityMesureCycle();
			}
			else
			{
				ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			}
			break;
		case 1:
			Log(Log::INFO)<<"Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[0]<<" temperature";
			setTemperatures(chargeThermalSteps[0]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			ret = charge(steps[0]);
			break;
		case 2:
			ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			break;
		case 3:
			Log(Log::INFO)<<"Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[0]<<" temperature";
			setTemperatures(chargeThermalSteps[1]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			ret = charge(steps[1]);
			break;
		case 4:
			ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			break;
		case 5:
			Log(Log::INFO)<<"Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[0]<<" temperature";
			setTemperatures(chargeThermalSteps[2]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			ret = charge(steps[2]);
			break;
		case 6:
			ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			break;
		case 7:
			Log(Log::INFO)<<"Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[0]<<" temperature";
			setTemperatures(chargeThermalSteps[3]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			ret = charge(steps[3]);
			break;
		case 8:
			ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			break;
		case 9:
			Log(Log::INFO)<<"Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[0]<<" temperature";
			setTemperatures(chargeThermalSteps[4]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			ret = charge(steps[4]);
			break;
		case 10:
			ret = thermalCycle(CYCLES_PER_STEP, startsubstep);
			break;
		case 11:
			Log(Log::INFO)<<"Starting multi Charing for globalstep "<<globalstep<<" will be executed at "<<chargeThermalSteps[5]<<" temperature";
			setTemperatures(chargeThermalSteps[5]);
			ret = heaters->allReady();
			if(!ret)
			{
				Log(Log::ERROR)<<"Not all heaters moved to ready state in globalstep"<<globalstep;
				break;
			}
			for(size_t i = 0; i < CYCLES_PER_STEP; ++i)
			{
				ret = charge(1);
				if(!ret)
				{
					Log(Log::ERROR)<<"Unable to charge in "<<globalstep;
					break;
				}
				ret = charge(0);
				if(!ret)
				{
					Log(Log::ERROR)<<"Unable to discharge in "<<globalstep;
					break;
				}
			}
			break;
		break;
		default:
			Log(Log::ERROR)<<"Unkown step "<<(globalstep % STEP_COUNT)<<'\n';
			break;
	}
	return ret;
}
