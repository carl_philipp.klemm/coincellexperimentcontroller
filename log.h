/**
* eisgenerator
* Copyright (C) 2021 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#pragma once
#include <iostream>
#include <string>
#include <mutex>
#include <sstream>
#include <ctime>
#include <iomanip>

class Log 
{
public:
	
	enum Level
	{
		DEBUG,
		INFO,
		WARN,
		ERROR,
		NEVER
	};

private:
	bool opened = false;
	Level msglevel = DEBUG;
	bool endline = true;
	bool time = true;

	static std::stringstream buffer;
	static bool sendmailEnabled;
	static std::string server;
	static std::string port;
	static std::string username;
	static std::string password;
	static std::string fromName;
	static std::string reportingAddr;

	static std::mutex mutex;

	std::string getLabel(Level level);

	bool sendmail(const std::string& subj, const std::string& body);
	
public:

	static bool headers;
	static Level level;
	static Level sendmailLevel;

	Log();
	Log(Level type, bool endlineI = true, bool time = true);
	~Log();

	static void setupSendmail(std::string serverIn, std::string portIn, std::string usernameIn,
							  std::string passwordIn, std::string fromNameIn, std::string reportingAddrIn);
	
	template<class T> Log &operator<<(const T &msg)
	{
		if(msglevel >= level) 
		{
			if(!opened)
			{
				opened = true;

				if(time)
				{
					time_t timestamp = std::time(nullptr);
					time_t t = std::time(nullptr);
					auto tm = *std::localtime(&t);
					std::cout<<std::put_time(&tm, "%Y-%m-%d %H:%M:%S")<<": ";
					buffer<<std::put_time(&tm, "%Y-%m-%d %H:%M:%S")<<": ";
				}
			}

			std::cout<<msg;
			buffer<<msg;
		}
		return *this;
	}
};

