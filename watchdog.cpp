#include "watchdog.h"

#include <thread>
#include <atomic>
#include <cstdio>

#include "log.h"

namespace watchdog
{

static std::atomic<long> timer = 0;
static std::atomic<bool> join = false;
static std::thread* thread = nullptr;
static unsigned long timeout;

static void threadFn()
{
	Log(Log::INFO)<<"Watchdog started with timeout "<<timeout;
	while(!join)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		if(timer-- < 0)
		{
			Log(Log::ERROR)<<"Watchdog timeout, exiting";
			std::exit(2);
		}
	}
	Log(Log::INFO)<<"Watchdog shutdown";
}

void reset()
{
	timer = timeout;
}

void init(unsigned long timeoutIn = 600)
{
	exit();
	timeout = timeoutIn;
	timer = timeout;
	join = false;
	thread = new std::thread(&watchdog::threadFn);
}

void exit()
{
	join = true;
	if(thread)
		thread->join();
	thread = nullptr;
}

}
