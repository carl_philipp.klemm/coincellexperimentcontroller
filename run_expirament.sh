#!/bin/bash

OUTDIR=$(date +run-%y-%m-%d)
LOGFILE=expiramentstdout.log
STEP=0
SUBSTEP=0
STOP=0
STEPFILE=/tmp/cchstep
RESTART=0

if [[ $# > 0 ]]; then
	echo "Will load first step from $STEPFILE"
	STEP=$(awk -F, '{ print $1 }' $STEPFILE)
	if [ $? != 0 ]; then
			echo "could not read last state from $STEPFILE"
			STOP=1
			break
	fi
	SUBSTEP=$(awk -F, '{ print $2 }' $STEPFILE)
	if [ $? != 0 ]; then
			echo "could not read last state from $STEPFILE"
			STOP=1
			break
	fi
fi

while [[ $STOP == 0 && RESTART -lt 100 ]]; do

	echo "Starting expirament at step $STEP substep $SUBSTEP"
	rm $STEPFILE || true

	coincellexpirament -w -l 10 -c "0,1,2,3,4,5,6" -o "$OUTDIR-$RESTART" -s $STEP -k $SUBSTEP -f $STEPFILE
	RET=$?
	RESTART=$((RESTART+1))

	echo "expirament stoped with $RET"
	if [ $RET == 0 ]; then
			STOP=1
	else
		STEP_AT_EXIT=$(awk -F, '{ print $1 }' $STEPFILE)
		if [ $? != 0 ]; then
			echo "could not read last state from $STEPFILE"
			STOP=1
			break
		fi

		SUBSTEP_AT_EXIT=$(awk -F, '{ print $2 }' $STEPFILE)
		if [ $? != 0 ]; then
			echo "could not read last state from $STEPFILE"
			STOP=1
			break
		fi

		if [ $STEP_AT_EXIT -gt $STEP ] || [ $SUBSTEP_AT_EXIT -gt $SUBSTEP ]; then
			echo "expirament crashed at globalsetp $STEP_AT_EXIT substep $SUBSTEP_AT_EXIT restarting"
			STEP=$STEP_AT_EXIT
			SUBSTEP=$SUBSTEP_AT_EXIT
		else
			echo "expirament crashed at globalsetp $STEP_AT_EXIT substep $SUBSTEP_AT_EXIT and made no progress, aborting"
			STOP=1
		fi
	fi
done

psucontrol /dev/ttyUSB0 enable 0 || true
coincellhell_cli -s 1 disable || true
coincellhell_cli -s 2 disable || true
coincellhell_cli -s 3 disable || true
coincellhell_cli -s 4 disable || true
coincellhell_cli -s 5 disable || true
coincellhell_cli -s 6 disable || true
eismultiplexer_cli -s 1 clear || true
eismultiplexer_cli -s 2 clear || true
eismultiplexer_cli -s 3 clear || true
eismultiplexer_cli -s 4 clear || true
eismultiplexer_cli -s 5 clear || true
rm $STEPFILE || true
