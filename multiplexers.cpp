#include "multiplexers.h"

#include <eismultiplexer.h>

#include "startupfailure.h"

Multiplexers::Multiplexers(const std::vector<uint16_t> serials)
{
	devices.resize(serials.size());

	Log(Log::INFO)<<"inializeing "<<serials.size()<<" mulitplexers with "<<serials.size()*CH_PER_DEVICE<<" channels";

	for(size_t i = 0; i < serials.size(); ++i)
	{
		int ret = eismultiplexer_connect(&devices[i].device, serials[i]);
		if(ret != 0)
			throw startup_failure("Unable to connect to mulitplexer with serial " + std::to_string(serials[i]));
		devices[i].mutex = std::make_unique<std::mutex>();
		Log(Log::INFO)<<"mulitplexer device "<<i<<" with serial "<<serials[i]<<" reporting";
	}

	disconnectAll();
}

channel_t Multiplexers::chNumberToCh(int ch)
{
	switch(ch)
	{
		case 0:
			return CHANNEL_A;
		case 1:
			return CHANNEL_B;
		case 2:
			return CHANNEL_C;
		case 3:
			return CHANNEL_D;
		case 4:
			return CHANNEL_E;
		case 5:
			return CHANNEL_F;
		case 6:
			return CHANNEL_G;
		default:
			return CHANNEL_NONE;
	}
}

std::string Multiplexers::chToString(channel_t ch)
{
	switch(ch)
	{
		case CHANNEL_A:
			return "A";
		case CHANNEL_B:
			return "B";
		case CHANNEL_C:
			return "C";
		case CHANNEL_D:
			return "D";
		case CHANNEL_E:
			return "E";
		case CHANNEL_F:
			return "F";
		case CHANNEL_G:
			return "G";
		case CHANNEL_NONE:
			return "NONE";
		default:
			return "invalid or multiple";
	}
}

bool Multiplexers::connectAll()
{
	for(Device& device : devices)
	{
		device.mutex->lock();
		int ret = eismultiplexer_connect_channel(&device.device,
									(channel_t)(CHANNEL_A | CHANNEL_B | CHANNEL_C | CHANNEL_D | CHANNEL_E | CHANNEL_F | CHANNEL_G));
		device.mutex->unlock();

		if(ret != 0)
			return false;
	}
	return true;
}

bool Multiplexers::disconnectAll()
{
	for(Device& device : devices)
	{
		device.mutex->lock();
		int ret = eismultiplexer_disconnect_channel(&device.device, (channel_t)(CHANNEL_NONE));
		device.mutex->unlock();

		if(ret != 0)
			return false;
	}
	return true;
}

bool Multiplexers::setCh(int ch, bool connected)
{
	std::pair<size_t, channel_t> addr = getAddress(ch);

	Log(Log::DEBUG)<<"Setting channel "<<chToString(addr.second)<<" on device "
		<<addr.first<<" to "<<(connected ? "connected" : "disconnected");

	devices[addr.first].mutex->lock();
	int ret = 0;
	if(connected)
		ret = eismultiplexer_connect_channel(&devices[addr.first].device, addr.second);
	else
		ret = eismultiplexer_disconnect_channel(&devices[addr.first].device, addr.second);
	devices[addr.first].mutex->unlock();

	return ret == 0;
}

std::pair<size_t, channel_t> Multiplexers::getAddress(int ch) const
{
	size_t device = ch/CH_PER_DEVICE;
	channel_t decodedChannel = chNumberToCh(ch % CH_PER_DEVICE);
	return {device, decodedChannel};
}

bool Multiplexers::setChExlusive(int ch, bool connected)
{
	std::pair<size_t, channel_t> addr = getAddress(ch);

	Log(Log::DEBUG)<<"For ch "<<ch<<": exclusively setting channel "<<chToString(addr.second)<<" on device "
		<<addr.first<<" to "<<(connected ? "connected" : "disconnected");

	if(!disconnectAll())
		return false;

	devices[addr.first].mutex->lock();
	int ret = eismultiplexer_connect_channel_exclusive(&devices[addr.first].device, addr.second);
	devices[addr.first].mutex->unlock();

	if(ret != 0)
		return false;

	return true;
}

size_t Multiplexers::chCount()
{
	return devices.size()*CH_PER_DEVICE;
}

std::vector<bool> Multiplexers::getConnected()
{
	std::vector<bool> connectedChannels(chCount(), false);

	for(size_t i = 0; i < devices.size(); ++i)
	{
		devices[i].mutex->lock();
		channel_t channels = eismultiplexer_get_connected(&devices[i].device);
		devices[i].mutex->unlock();
		connectedChannels[i*CH_PER_DEVICE+0] = channels & CHANNEL_A;
		connectedChannels[i*CH_PER_DEVICE+1] = channels & CHANNEL_B;
		connectedChannels[i*CH_PER_DEVICE+2] = channels & CHANNEL_C;
		connectedChannels[i*CH_PER_DEVICE+3] = channels & CHANNEL_D;
		connectedChannels[i*CH_PER_DEVICE+4] = channels & CHANNEL_E;
		connectedChannels[i*CH_PER_DEVICE+5] = channels & CHANNEL_F;
		connectedChannels[i*CH_PER_DEVICE+6] = channels & CHANNEL_G;
	}
	return connectedChannels;
}

bool Multiplexers::log(Log::Level level)
{
	std::vector<bool> connectedChannels = getConnected();

	Log(level, false)<<"Multiplexer channels: ";
	for(size_t i = 0; i < connectedChannels.size(); ++i)
		Log(level, false, false)<<(connectedChannels[i] ? "1, " : "0, ");
	Log(level, true, false)<<"";

	return true;
}
