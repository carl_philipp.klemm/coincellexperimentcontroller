#pragma once

#include <vector>
#include <coincellhell/coincellhell.h>
#include <cstdint>
#include <functional>
#include <mutex>
#include <memory>
#include <set>

#include "log.h"

class Heaters
{
public:

	static constexpr int ERR_RECOVERED = 0;
	static constexpr int ERR_CONNECTION = 1;
	static constexpr int ERR_FAIL_DETECT = 2;
	static constexpr int ERR_OVERTEMP = 3;
	static constexpr int HEATERS_PER_DEVICE = 4;

	static constexpr float PANIC_TEMPERATURE = 110.0f;

	inline static bool mock = false;

private:

	struct device
	{
		struct coincellhell device;
		std::unique_ptr<std::mutex> mutex;
		uint16_t serial;
		bool bad;
		bool badHeater[HEATERS_PER_DEVICE];
	};

	std::vector<struct device> devices;
	bool watchdogs = false;

	std::function<void(int device, uint16_t serial, int code)> deviceFailCb;
	std::function<void(int heater, int device)> heaterFailCb;

	void deviceError(int device, int reason);
	bool deviceBad(int device);
	bool heaterBad(int heater);
	void recoverDevice(int device);

	bool getHeaterState(int heater, struct heater_state& state);

public:

	Heaters(const std::vector<uint16_t> serials,
		std::function<void(int device, uint16_t serial, int code)> deviceFailCb,
		std::function<void(int heater, int device)> heaterFailCb, const std::set<uint16_t>& maskedHeaters = {});
	bool wait(int timeout = -1);
	bool allReady();
	std::vector<bool> getBadDevices();
	void reconnectDevices();

	bool setHeaterEnabled(int heater, bool enabled);
	bool getHeaterEnabled(int heater);
	bool setSetpoint(int heater, float temperature);
	bool getSetpoint(int heater, float& temperature);
	bool setRamp(int heater, time_t endTime, float temperature);
	bool getTemperature(int heater, float& temperature);
	bool setMaxCurrent(uint8_t current);
	std::pair<int, int> getAddress(int heater) const;
	int count();
	void enableWatchdogs();

	void log(Log::Level level = Log::INFO);
	void safetyCheck();
};
