#pragma once

#include "heaters.h"
#include "multiplexers.h"

#include <vector>
#include <filesystem>
#include <eisgenerator/eistype.h>

#include "biocontrol.h"

class CoinCell
{
private:
	int heaterId;
	std::vector<int> multiplexerChs;
	Heaters* heaters;
	Multiplexers* multiplexers;
	double lastKnownCap = -1;

	static BioControl biocontrol;

	inline static int channelGrabbed = -1;

public:

	CoinCell(Heaters* heaters, Multiplexers* multiplexers, int heaterId, const std::vector<int> multiplexerChs);

	bool connectExclusive();
	bool dissconnect();
	bool setConnected(bool connected);
	void setTemperature(float temperature);
	bool getTemperature(float& temperature);
	bool getSetpoint(float& temperature);
	bool measureOcv(float& ocv);
	bool isShorted(bool& detected);
	bool measureEis(std::filesystem::path path, std::string userStr = "");
	bool setEnabled(bool enable);
	bool getEnabled();
	double getLastKnownCapacity();
	void setLastKnownCapacity(double cap);
	double getFullVoltage();
	double getEmptyVoltage();
	int getHeaterId() const;
	const std::vector<int>& getMulitplexerChannels() const;

	static void setBiocontrol(const BioControl& biocontrol);
};
