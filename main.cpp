#include <cstdint>
#include <eismultiplexer.h>
#include <iostream>
#include <memory>
#include <thread>
#include <libserialport.h>
#include <sstream>
#include <thread>
#include <filesystem>
#include <fstream>
#include <vector>
#include <libserialport.h>
#include <sys/stat.h>
#include <fstream>


#include "biocontrol.h"
#include "coincell.h"
#include "coincells.h"
#include "log.h"
#include "heaters.h"
#include "multiplexers.h"
#include "randomgen.h"
#include "vcpps.h"
#include "startupfailure.h"
#include "panic.h"
#include "options.h"
#include "expirament.h"
#include "watchdog.h"

void devicefailed(Vcpps& psu, int device, uint16_t serial, int code)
{
	std::stringstream ss;
	Log(Log::WARN)<<"Heater device "<<device<<" with serial number "<<serial<<" has "
		<<(code == Heaters::ERR_RECOVERED ? "recovered" : "failed")<<" with code "<<code;
	if(code == Heaters::ERR_OVERTEMP)
	{
		Log(Log::ERROR)<<"Critical overtemp on device "<<device<<" with serial number "<<serial<<"\n\nEMERGENCY SHUTDOWN!!";
		panic(psu);
	}
}

void heaterfailed(int heater, int device)
{
	Log(Log::WARN)<<"Heater "<<heater<<" on device "<<device<<" has failed";
}

static void safetyThreadFn(Heaters* heaters)
{
	static unsigned int step = 0;
	Log(Log::INFO)<<"Safety thread has started";
	while(!stop)
	{
		Log(Log::DEBUG)<<"Safety step "<<step++;
		heaters->safetyCheck();
		std::this_thread::sleep_for(std::chrono::seconds(10));
	}
}

static void loggingThreadFn(Heaters* heaters, Multiplexers* multiplexers,
							Vcpps* psu, Expirament* expirament, const std::filesystem::path outdir)
{
	static unsigned int step = 0;
	Log(Log::INFO)<<"Logging thread has started";

	std::filesystem::path logfile = outdir/"expiramentlog.csv";

	std::fstream file(logfile, std::ios_base::out);
	if(!file.is_open())
	{
		Log(Log::ERROR)<<"Unable to open logfile "<<logfile;
		stop = true;
		fail = true;
		return;
	}

	file<<"time,globalstep,voltage,current,curent_limited,";
	for(int i = 0; i < heaters->count(); ++i)
		file<<"heater"<<i<<',';
	file<<std::endl;

	while(!stop)
	{
		Log(Log::DEBUG)<<"Logging step "<<step++;

		Vcpps::Status psustatus;
		bool ret = psu->getStatus(psustatus);
		Log(Log::DEBUG, false)<<"PSU: ";

		if(!ret)
		{
			Log(Log::ERROR)<<"Could not read psu!";
		}
		else
		{
			Log(Log::DEBUG, false)<<"Voltage: "<<psustatus.voltage;
			Log(Log::DEBUG, false)<<" Current: "<<psustatus.current;
			Log(Log::DEBUG)<<" Current limited: "<<(psustatus.curent_limited ? "true" : "false");
		}
		multiplexers->log(Log::DEBUG);
		heaters->log(Log::DEBUG);

		time_t now = time(NULL);

		file<<now<<','<<expirament->getGlobalstep()<<','<<psustatus.voltage<<','<<psustatus.current<<','<<psustatus.curent_limited<<',';

		for(int i = 0; i < heaters->count(); ++i)
		{
			float temp = -1;
			heaters->getTemperature(i, temp);
			file<<temp<<',';
		}
		file<<std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(10));
	}
}

static void currentMonitorThreadFn(Heaters* heaters, Vcpps* psu, float minVoltage)
{
	uint8_t maxCurrent = 255;
	heaters->setMaxCurrent(maxCurrent);
	while(!stop)
	{
		Vcpps::Status status;
		bool ret = psu->getStatus(status);

		if(maxCurrent < 255 && !status.curent_limited)
		{
			maxCurrent = std::min(255, maxCurrent+20);
			Log(Log::INFO)<<"Voltage has recovered at "<<status.voltage<<". Max current now: "<<static_cast<int>(maxCurrent);
			heaters->setMaxCurrent(maxCurrent);
		}
		else if(status.voltage < minVoltage && maxCurrent > 100 && status.curent_limited)
		{
			maxCurrent -= 20;
			Log(Log::INFO)<<"Voltage low at "<<status.voltage<<", sheading load. Max current now: "<<static_cast<int>(maxCurrent);
			heaters->setMaxCurrent(maxCurrent);
		}
		std::this_thread::sleep_for(std::chrono::seconds(10));
	}
}

bool listSerialPorts()
{
	struct sp_port** ports;

	sp_return ret = sp_list_ports(&ports);
	if(ret != SP_OK)
	{
		Log(Log::ERROR)<<"Unable to list serial ports";
		return false;
	}

	for(size_t i = 0; ports[i] != nullptr; ++i)
	{
		enum sp_transport transport = sp_get_port_transport(ports[i]);

		if(transport != SP_TRANSPORT_USB)
			continue;

		Log(Log::INFO)<<"USB serial port "<<i;
		Log(Log::INFO)<<"\tManufacturer: "<<sp_get_port_usb_manufacturer(ports[i]);
		Log(Log::INFO)<<"\tProduct: "<<sp_get_port_usb_product(ports[i]);
		Log(Log::INFO)<<"\tSerial: "<<sp_get_port_usb_serial(ports[i]);
		int vid;
		int pid;
		sp_get_port_usb_vid_pid(ports[i], &vid, &pid);
		Log(Log::INFO)<<std::hex<<"\tVID: "<<vid<<" PID: "<<pid<<std::dec;
	}

	sp_free_port_list(ports);

	return true;
}

int main(int argc, char** argv)
{
	umask(0);
	Log::level = Log::INFO;
	Log::sendmailLevel = Log::WARN;

	rd::init();
	watchdog::init(10*60*60);

	Config config;
	argp_parse(&argp, argc, argv, 0, 0, &config);

	Log(Log::INFO)<<"CoinCellExpierament 1.0";
	Log(Log::INFO)<<"Running startup sequence";

	listSerialPorts();

	if(!std::filesystem::is_directory(config.outdir))
	{
		if(!std::filesystem::create_directory(config.outdir))
		{
			Log(Log::ERROR)<<"Could not create a directory at "<<config.outdir<<" for logging";
			return 1;
		}
	}

	if(!config.cells.empty())
	{
		Log(Log::INFO, false)<<"Using only cells ";
		for(uint16_t id : config.cells)
			Log(Log::INFO, false, false)<<id<<',';
		Log(Log::INFO, true, false)<<"";
	}

	bool ret = true;

	try
	{
		BioControl biocontrol(config.biocontrolpath, config.bioip);
		CoinCell::setBiocontrol(biocontrol);

		Log(Log::INFO)<<"Geting PSU";
		Vcpps psu(config.psuPort);
		psu.setEnabled(false);

		Log(Log::INFO)<<"Aquireing heaters and multiplexers";
		Heaters heaters(config.heaterSerials,
						[&psu](int device, uint16_t serial, int code){devicefailed(psu, device, serial, code);},
						&heaterfailed, config.maskedHeaters);
		Multiplexers multiplexers(config.multiplexerSerials);

		std::vector<std::unique_ptr<CoinCell>> coinCells = asign_coincells(config.cells, &heaters, &multiplexers);

		Expirament expirament(&coinCells, &psu, config.psuVoltage, config.psuCurrent,
							  &heaters, &multiplexers, &biocontrol, config.outdir, config.stepfile);

		Log(Log::INFO)<<"Starting Threads";
		std::thread safetyThread(safetyThreadFn, &heaters);
		std::thread loggingThread(loggingThreadFn, &heaters, &multiplexers, &psu, &expirament, config.outdir);
		std::thread currentThread(currentMonitorThreadFn, &heaters, &psu, 5);

		if(!config.skipstart)
			ret = expirament.startup();

		if(!ret)
			return 1;

		Log(Log::INFO)<<"Enableing email system";
		//Log::setupSendmail("smtp.strato.de", "465", "experiments@rhd-instruments.de", "RmUE5jBRkxCGZ1vpa65a", "Kiss Expirament", "carl@uvos.xyz");

		std::this_thread::sleep_for(std::chrono::seconds(5));
		if(stop || fail)
		{
			Log(Log::ERROR)<<"Failure in a thread detected, abort";
			return 2;
		}

		if(config.watchdog)
		{
			Log(Log::INFO)<<"Enableing mcu watchdogs";
			heaters.enableWatchdogs();
		}

		ret = expirament.run(config.globalStep, config.subStep);

		expirament.shutdown();

		stop = true;
		safetyThread.join();
		loggingThread.join();
		currentThread.join();
	}
	catch(const startup_failure& ex)
	{
		Log(Log::ERROR)<<"Unable to startup: "<<ex.what();
		return 1;
	}

	return ret ? 0 : 1;
}
