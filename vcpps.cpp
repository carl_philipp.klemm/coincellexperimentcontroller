#include "vcpps.h"

#include "startupfailure.h"
#include <cstdint>
#include <libserialport.h>
#include <string>
#include <sstream>

#include "log.h"

Vcpps::Vcpps(const std::string& portName)
{
	std::lock_guard<std::mutex> guard(portMutex);
	sp_return ret = sp_get_port_by_name(portName.c_str(), &port);
	if(ret != SP_OK)
		throw startup_failure("No serial port with the name " + portName + " found");
	ret = sp_open(port, SP_MODE_READ_WRITE);
	if(ret != SP_OK)
	{
		sp_free_port(port);
		throw startup_failure("Could not open serial port " + std::string(sp_last_error_message()));
	}
	sp_set_baudrate(port, 9600);
	sp_set_bits(port, 8);
	sp_set_stopbits(port, 1);
	sp_set_flowcontrol(port, SP_FLOWCONTROL_NONE);
}

Vcpps::~Vcpps()
{
	if(port)
		sp_free_port(port);
}

Vcpps::Vcpps(const Vcpps& in)
{
	operator=(in);
}

bool Vcpps::waitForOk()
{
	char buffer[4] = {};
	int ret = sp_blocking_read(port, buffer, 3, 3000);
	if(ret < 3)
	{
		Log(Log::ERROR)<<"Did not get ok from psu got: "<<ret<<' '<<(ret < 0 ? sp_last_error_message() : "");
		return false;
	}
	sp_drain(port);

	bool buffercheck = std::string(buffer) == "OK\r";
	if(!buffercheck)
		Log(Log::ERROR)<<"Buffer is "<<buffer<<" instead of OK";
	return buffercheck;
}

Vcpps& Vcpps::operator=(const Vcpps& in)
{
	std::lock_guard<std::mutex> guard(portMutex);
	if(port)
		sp_free_port(port);
	if(in.port)
		sp_copy_port(in.port, &port);
	return *this;
}

bool Vcpps::setEnabled(bool enabled)
{
	std::string buff(COMMAND_OUTPUT_DISABLE);
	buff.append(std::to_string(!enabled));
	buff.push_back('\r');
	std::lock_guard<std::mutex> guard(portMutex);
	sp_nonblocking_write(port, buff.c_str(), buff.size());
	return waitForOk();
}

bool Vcpps::setVoltage(float voltage)
{
	if(voltage > 18.0f || voltage < 1.0f)
		return false;

	uint16_t voltageInt = voltage*10.0f;

	std::stringstream ss;
	ss<<COMMAND_VOLTAGE<<std::setw(3)<<std::setfill('0')<<voltageInt<<'\r';
	Log(Log::DEBUG)<<"Writeing "<<ss.str()<<" to psu";
	std::lock_guard<std::mutex> guard(portMutex);
	sp_nonblocking_write(port, ss.str().c_str(), ss.str().size());
	return waitForOk();
}

bool Vcpps::setCurrent(float current)
{
	if(current > 10.0f || current < 0.1f)
		return false;

	uint16_t currentInt = current*10.0f;
	std::stringstream ss;
	ss<<COMMAND_CURRENT<<std::setw(3)<<std::setfill('0')<<currentInt<<'\r';
	Log(Log::DEBUG)<<"Writeing "<<ss.str()<<" to psu";
	std::lock_guard<std::mutex> guard(portMutex);
	sp_nonblocking_write(port, ss.str().c_str(), ss.str().size());
	return waitForOk();
}

bool Vcpps::getStatus(Vcpps::Status& status)
{
	char buf[11] = {};
	std::string outBuf(COMMAND_STATUS);
	outBuf.push_back('\r');
	std::lock_guard<std::mutex> guard(portMutex);
	sp_blocking_write(port, outBuf.c_str(), outBuf.size(), 300);
	int ret = sp_blocking_read(port, buf, 10, 300);
	if(ret < 10)
	{
		Log(Log::ERROR)<<port<<" sp_blocking_read retuned "<<ret<<' '<<sp_last_error_message();
		return false;
	}

	std::string currentStr;
	currentStr.push_back(buf[4]);
	currentStr.push_back(buf[5]);
	currentStr.push_back('.');
	currentStr.push_back(buf[6]);
	currentStr.push_back(buf[7]);

	std::string voltageStr;
	voltageStr.push_back(buf[0]);
	voltageStr.push_back(buf[1]);
	voltageStr.push_back('.');
	voltageStr.push_back(buf[2]);
	voltageStr.push_back(buf[3]);

	try
	{
		status.voltage = std::stof(voltageStr);
		status.current = std::stof(currentStr);
		status.curent_limited = buf[8] == '1';
	}
	catch(std::invalid_argument& ex)
	{
		sp_drain(port);
		Log(Log::ERROR)<<"Conversion from psu return to value failed buffer:"<<buf;
		Log(Log::ERROR)<<"Voltage was given as: "<<voltageStr;
		Log(Log::ERROR)<<"Current was given as: "<<currentStr;
		return false;
	}

	return waitForOk();
}
