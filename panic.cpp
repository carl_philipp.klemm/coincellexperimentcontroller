#include "panic.h"

bool stop = false;
bool fail = false;

void panic(Vcpps& psu)
{
	psu.setEnabled(false);
	stop = true;
	fail = true;
}
