#pragma once

#include <eismultiplexer.h>
#include <vector>
#include <mutex>
#include <memory>

#include "log.h"

class Multiplexers
{
private:

	static constexpr int CH_PER_DEVICE = 7;

	struct Device
	{
		eismultiplexer device;
		std::unique_ptr<std::mutex> mutex;
	};

	std::vector<Device> devices;

	static channel_t chNumberToCh(int ch);


public:
	Multiplexers(const std::vector<uint16_t> serials);
	bool connectAll();
	bool disconnectAll();
	bool setCh(int ch, bool connected);
	bool setChExlusive(int ch, bool connected);
	std::pair<size_t, channel_t> getAddress(int ch) const;
	size_t chCount();
	std::vector<bool> getConnected();
	bool log(Log::Level level);

	static std::string chToString(channel_t ch);
};
