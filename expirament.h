#pragma once

#include "coincell.h"
#include "biocontrol.h"
#include "heaters.h"
#include "multiplexers.h"
#include "vcpps.h"

#include <atomic>

class Expirament
{
private:
	static constexpr size_t MAX_LOOP = 100;
	static constexpr size_t STEP_COUNT = 12;

	std::vector<std::unique_ptr<CoinCell>>* coinCells;
	Vcpps* psu;
	Heaters* heaters;
	Multiplexers* multiplexers;
	BioControl* biocontrol;
	float voltage;
	float current;
	std::filesystem::path outdir;
	std::filesystem::path stepfile;
	std::atomic<size_t> globalstep = 0;

	bool preparePsu();

	bool step(size_t startsubstep = 0);

	void setTemperatures(float fraction);

	bool takeMesurements(bool onlyTVariing = false);

	bool heatMeasureSeq(float fraction);

	bool thermalCycle(size_t count, size_t start = 0);

	bool charge(float fraction, bool all = false);

	bool chargeCellToVoltage(int id, float voltage, const std::string& prefix);

	void saveStep(size_t substep = 0);

	bool capacityMesureCycle();

	std::filesystem::path createCsvPath(const std::filesystem::path& filename);

	static std::vector<double> createStepSequence(size_t upSteps, size_t downSteps, double low = 0, double high = 1);

public:

	Expirament(std::vector<std::unique_ptr<CoinCell>>* coinCells, Vcpps* psu, float voltage, float current,
		Heaters* heaters, Multiplexers* multiplexers, BioControl* biocontrol, const std::filesystem::path& outdir,
		const std::filesystem::path& stepfile);

	bool startup();
	bool run(size_t startstep = 0, size_t startsubstep = 0);
	bool shutdown();

	size_t getGlobalstep();
};
