#include "coincells.h"

#include "log.h"

std::vector<std::unique_ptr<CoinCell>> asign_coincells(std::vector<uint16_t> cellids, Heaters* heaters, Multiplexers* multiplexers)
{
	CoinCell cell(heaters, multiplexers, 0, {3});
	std::vector<std::unique_ptr<CoinCell>> coinCells;

	/* Cell distrobution:
	 * Shelf 0: 0  1  2  3   |  4  5  6
	 * Shelf 1: 7  8  9  10  |  11 12 13
	 * Shelf 2: 14 15 16 17  |  18 19 20
	 *
	 * Groups:
	 * Shelf 0: 0 | 1
	 * Shelf 1: 2 | 3
	 * Shelf 2: 4 | 5
	 */

	// Shelf 0
	// Group 0
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 0, std::vector<int>({3, 10})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 1, std::vector<int>({2, 9})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 2, std::vector<int>({1, 8})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 3, std::vector<int>({0, 7})));

	// Group 1
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 4, std::vector<int>({6, 13})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 5, std::vector<int>({5, 12})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 6, std::vector<int>({4, 11})));

	// Shelf 1
	// Group 2
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 8, std::vector<int>({24, 17})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 9, std::vector<int>({23, 16})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 10, std::vector<int>({22, 15})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 11, std::vector<int>({21, 14})));

	// Group 3
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 12, std::vector<int>({27, 20})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 13, std::vector<int>({26, 19})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 14, std::vector<int>({25, 18})));

	// Shelf 2
	// Group 4
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 16, std::vector<int>({31})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 17, std::vector<int>({30})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 18, std::vector<int>({29})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 19, std::vector<int>({28})));

	// Group 5
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 20, std::vector<int>({34})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 21, std::vector<int>({33})));
	coinCells.push_back(std::make_unique<CoinCell>(heaters, multiplexers, 22, std::vector<int>({32})));

	if(!cellids.empty())
	{
		for(ssize_t i = coinCells.size()-1; i >= 0; --i)
		{
			auto search = std::find(cellids.begin(), cellids.end(), i);
			if(search == cellids.end())
				coinCells.erase(coinCells.begin()+i);
		}
	}

	Log(Log::INFO)<<"Connections:";
	size_t i = 0;
	for(std::unique_ptr<CoinCell>& cell : coinCells)
	{
		int heater = cell->getHeaterId();
		const std::vector<int>& muliplexerchs = cell->getMulitplexerChannels();

		std::pair<int, int> heaterAddr = heaters->getAddress(heater);

		Log(Log::INFO, false)<<"Coin Cell "<<i<<", CoincellHell "<<heaterAddr.first<<" unit "<<heaterAddr.second<<" is connected to: ";

		for(int id : muliplexerchs)
		{
			std::pair<int, channel_t> addr = multiplexers->getAddress(id);
			Log(Log::INFO, false, false)<<" Mulitplexer "<<addr.first<<" channel "<<Multiplexers::chToString(addr.second)<<", ";
		}
		Log(Log::INFO, false, false)<<'\n';
		++i;
	}

	return coinCells;
}
