#pragma once

#include <filesystem>
#include <string>

class BioControl
{
private:
	void execBiocontrol(const std::string& command, const std::string& userstr, std::string& output, float voltage = -1, float current = 0);

	std::filesystem::path exepath;
	std::string ipAddr;

public:

	BioControl() = default;
	BioControl(const std::filesystem::path& exepath, const std::string& ipAddr);

	bool measure_eis(const std::filesystem::path& outPath, const std::string& userstr, bool retry = true);
	bool measure_ocv(float& ocv, bool retry = true);
	bool shorted(bool& shorted);
	bool charge(float fraction, float current, const std::filesystem::path& outPath, const std::string& userstr, bool retry = true);
	bool chargeToVoltage(float voltage, float current, const std::filesystem::path& outPath, const std::string& userstr, bool retry = true);
};
