#pragma once

#include "coincell.h"
#include "multiplexers.h"
#include "heaters.h"

std::vector<std::unique_ptr<CoinCell>> asign_coincells(std::vector<uint16_t> cellids, Heaters* heaters, Multiplexers* multiplexers);
