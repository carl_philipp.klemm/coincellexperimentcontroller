#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <argp.h>
#include <filesystem>
#include <set>

#include "log.h"
#include "tokenize.h"

const char *argp_program_version = "kissExpiramentController-1.0";
const char *argp_program_bug_address = "<carl@uvos.xyz>";
static char doc[] = "Application that controls the kiss expirament";
static char args_doc[] = "";

static struct argp_option options[] =
{
  {"verbose",     'v', 0,               0,  "Show debug messages" },
  {"quiet",       'q', 0,               0,  "only output data" },
  {"out",         'o', "[DIRECTORY]",   0,  "Directory where to save the output mesurements" },
  {"muliplexers", 'm', "[SERIALS]",     0,  "List the serial numbers of the involved multiplexers" },
  {"heaters",     'r', "[SERIALS]",     0,  "set the serial numbers of the involved coincellhells" },
  {"heater-mask" ,'x', "[SERIALS]",     0,  "disable the actual driveing of the given heaters" },
  {"psuport",     'p', "[PORT]",        0,  "sets the serial port where the psu is connected" },
  {"biocontrol",  'b', "[PATH]",        0,  "sets the path where the biocontrol application is stored" },
  {"bioip",       'i', "[IPADDR]",      0,  "the ip address of the biologic device" },
  {"cellmask",    'c', "[LIST OF IDS]", 0,  "lists the coin cell units to be involved in the expirament" },
  {"voltage",     'u', "[VOLTAGE]",     0,  "sets the voltage at wich to operate the heaters" },
  {"current",     'l', "[CURRENT]",     0,  "sets the current limit at wich to operate the heaters"},
  {"step",        's', "[STEP NUMBER]", 0,  "sets the global setp at wich to start the expirament"},
  {"sub-step",    'k', "[STEP NUMBER]", 0,  "sets the subsep of the expirament"},
  {"stepfile",    'f', "[PATH]",        0,  "if set this file is used to save the step and substep"},
  {"skip-start",  'z'  , 0,             0,  "skipps the startup sequence"},
  {"watchdog",    'w'  , 0,             0,  "enable mcu watchdogs"},
  { 0 }
};

struct Config
{
	std::vector<uint16_t> multiplexerSerials = {1, 2, 3, 4, 5};
	std::vector<uint16_t> heaterSerials = {1, 2, 3, 4, 5, 6};
	std::set<uint16_t> maskedHeaters;
	std::string psuPort = "/dev/ttyUSB0";
	std::filesystem::path biocontrolpath = "/home/philipp/programming/biocontrol/build/biocontrol.exe";
	std::string bioip = "10.2.0.2";
	std::vector<uint16_t> cells;
	std::filesystem::path outdir = "./log";
	std::filesystem::path stepfile = "/tmp/cchstep";
	float psuVoltage = 10;
	float psuCurrent = 5;

	size_t globalStep = 0;
	size_t subStep = 0;
	bool skipstart = false;
	bool watchdog = false;
};

static std::vector<uint16_t> parseList(const std::string& in)
{
	std::vector<std::string> tokens = tokenize(in, ',');
	std::vector<uint16_t> list;
	list.reserve(tokens.size());
	for(const std::string& str : tokens)
		list.push_back(std::stoul(str));

	return list;
}

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	Config *config = reinterpret_cast<Config*>(state->input);

	try
	{
		switch (key)
		{
		case 'q':
			Log::level = Log::ERROR;
			break;
		case 'v':
			Log::level = Log::DEBUG;
			break;
		case 'm':
			config->multiplexerSerials = parseList(arg);
			break;
		case 'r':
			config->heaterSerials = parseList(arg);
			break;
		case 'x':
		{
			std::vector<uint16_t> list = parseList(arg);
			config->maskedHeaters.insert(list.begin(), list.end());
			break;
		}
		case 'p':
			config->psuPort = arg;
			break;
		case 'b':
			config->biocontrolpath = arg;
			break;
		case 'i':
			config->bioip = arg;
			break;
		case 'c':
			config->cells = parseList(arg);
			break;
		case 'u':
			config->psuVoltage = std::stof(arg);
			break;
		case 'l':
			config->psuCurrent = std::stof(arg);
			break;
		case 's':
			config->globalStep = std::stoul(arg);
			break;
		case 'k':
			config->subStep = std::stoul(arg);
			break;
		case 'f':
			config->stepfile = arg;
			break;
		case 'o':
			config->outdir = arg;
			break;
		case 'z':
			config->skipstart = true;
			break;
		case 'w':
			config->watchdog = true;
			break;
		default:
			return ARGP_ERR_UNKNOWN;
		}
	}
	catch(const std::invalid_argument& ia)
	{
		argp_usage(state);
	}
	return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

