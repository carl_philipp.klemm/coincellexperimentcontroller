#include "coincell.h"
#include <chrono>
#include <thread>

CoinCell::CoinCell(Heaters* heatersIn, Multiplexers* multiplexersIn, int heaterIdIn, const std::vector<int> multiplexerChsIn):
heaters(heatersIn), multiplexers(multiplexersIn), heaterId(heaterIdIn), multiplexerChs(multiplexerChsIn)
{

}

bool CoinCell::connectExclusive()
{
	bool ret = multiplexers->disconnectAll();

	if(!ret)
		return ret;

	for(int id : multiplexerChs)
	{
		ret = multiplexers->setCh(id, true);
		if(!ret)
			return ret;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(250));
	return ret;
}

bool CoinCell::dissconnect()
{
	bool ret = true;
	for(int id : multiplexerChs)
	{
		ret = multiplexers->setCh(id, false);
		if(!ret)
			return ret;
	}
	return ret;
}

void CoinCell::setTemperature(float temperature)
{
	heaters->setSetpoint(heaterId, temperature);
}

bool CoinCell::measureOcv(float& ocv)
{
	connectExclusive();
	bool ret = biocontrol.measure_ocv(ocv);
	dissconnect();
	return ret;
}

void CoinCell::setLastKnownCapacity(double cap)
{
	lastKnownCap = cap;
}

double CoinCell::getLastKnownCapacity()
{
	return lastKnownCap;
}

bool  CoinCell::getTemperature(float& temperature)
{
	bool ret = heaters->getTemperature(heaterId, temperature);
	return ret;
}

bool CoinCell::getSetpoint(float& temperature)
{
	bool ret = heaters->getSetpoint(heaterId, temperature);
	return ret;
}

bool CoinCell::getEnabled()
{
	return heaters->getHeaterEnabled(heaterId);
}

bool  CoinCell::setConnected(bool connected)
{
	bool ret = true;
	for(int id : multiplexerChs)
	{
		ret = multiplexers->setCh(id, connected);
		if(!ret)
			return ret;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(250));
	return ret;
}

bool CoinCell::isShorted(bool& detected)
{
	connectExclusive();
	bool ret = biocontrol.shorted(detected);
	dissconnect();
	return ret;
}

bool CoinCell::measureEis(std::filesystem::path path, std::string userStr)
{
	connectExclusive();
	bool ret = biocontrol.measure_eis(path, userStr);
	dissconnect();
	return ret;
}

void CoinCell::setBiocontrol(const BioControl& biocontrolIn)
{
	biocontrol = biocontrolIn;
}

bool CoinCell::setEnabled(bool enable)
{
	return heaters->setHeaterEnabled(heaterId, enable);
}

double CoinCell::getFullVoltage()
{
	return 4.2;
}

double CoinCell::getEmptyVoltage()
{
	return 3.0;
}

int CoinCell::getHeaterId() const
{
	return heaterId;
}

const std::vector<int>& CoinCell::getMulitplexerChannels() const
{
	return multiplexerChs;
}

BioControl CoinCell::biocontrol;

