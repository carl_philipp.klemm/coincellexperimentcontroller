#pragma once

namespace watchdog
{

void init(unsigned long timeout);
void reset();
void exit();

}
