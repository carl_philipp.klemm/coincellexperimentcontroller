#pragma once

#include <cstddef>
namespace rd
{
	double rand(double max = 1, double min = 0);
	void init();
	size_t uid();

}
